package com.example.Ecommerce;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.registrationDAO.registrationdao;
import com.example.model.ecommerce;  
import com.example.model.*;

@RestController
@CrossOrigin("*")

public class ecommerceController {
	
	@Autowired
	registrationdao dao;
	@PostMapping("/login1")
	public String login (@RequestBody ecommerce user)throws Exception{
		ecommerce storedUser=dao.getUserByEmail(user.getEmail());
		System.out.print(storedUser);
		if(storedUser!=null && user.getPassword().equals(storedUser.getPassword())){
			return "true";
		}
		else{
			throw new Exception("Invaild credentials");
		}
	}

	@RequestMapping("showusers")
	public List<ecommerce> getAllusers(){
		return dao.getAllusers();
	}
	
	@RequestMapping("showElementById/{id}")
	public ecommerce getElementById(@PathVariable int id){
		return dao.getElementById(id);
	}
	

	@PostMapping("regEmp")
	public ecommerce registerDepartment(@RequestBody ecommerce emp){
		return dao.registrationuser(emp);
	}
	
	@PutMapping("updateEmp/{name}")
	public ecommerce updateDepartment(@PathVariable String name, @RequestBody ecommerce emp){
		return dao.updateByName(name,emp);
	}
	
	@DeleteMapping("deleteEmp/{id}")
	public ecommerce deleteDepartmentById(@PathVariable int id){
		return dao.deleteDepartmentById(id);
	}
	
	
	@GetMapping("/getUserByEmail/{email}")
    public ResponseEntity<userData> getUserByEmail(@PathVariable String email) {
        try {
            ecommerce user = dao.getUserByEmail(email);
            if (user != null) {
                userData userData = new userData(); // Create a UserData object from the retrieved ecommerce user data
                userData.setId(user.getId());
                userData.setName(user.getName());
                userData.setEmail(user.getEmail());
                userData.setNumber(user.getNumber());
                userData.setPassword(user.getPassword());
                // Map other properties from ecommerce to UserData as needed
                return new ResponseEntity<>(userData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND); // User not found
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); // Error retrieving user data
        }
    }
}
	
