package com.example.Ecommerce;




import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages="com")
@EnableJpaRepositories(basePackages = "com.example.registrationDAO")
@EntityScan(basePackages = "com.example.model")
public class ecommerceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ecommerceApplication.class, args);
        System.out.println("ecommerce");
    }

}