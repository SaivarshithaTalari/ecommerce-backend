package com.example.registrationDAO;





import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository; 

import com.example.model.*;

@Repository
public interface registrationrespository extends JpaRepository<ecommerce,Integer>{

	Optional<ecommerce> findByEmail(String email);

	Optional<ecommerce> findByName(String name);



	
	
	
//	@Query("from ecommerce e where e.email = :email and e.password=:password ")
//	ecommerce getByEmailAndPassword(@Param ("email") String email,@Param ("password") String password );
//	
//	ecommerce findByEmailAndPassword(String email, String password);
		
//	@Query("from Product p where p.ProductName=:pName")
//	Product findByName(@Param("pName")String name);
}
