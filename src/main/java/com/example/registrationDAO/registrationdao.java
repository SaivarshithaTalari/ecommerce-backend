package com.example.registrationDAO;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.registrationDAO.registrationrespository;
import com.example.model.ecommerce;
@Service
public class registrationdao {
	@Autowired
	registrationrespository comRepo;

	
	public List<ecommerce> getAllusers() {
		return comRepo.findAll();
	}


	public ecommerce getElementById(int id) {
		return comRepo.findById(id).orElse(new ecommerce());
	}


	public ecommerce registrationuser(ecommerce emp) {
		return comRepo.save(emp);
	}


	public ecommerce updateuser(ecommerce emp) {
		return comRepo.save(emp);
	}


	public void deleteuserById(int id) {
		comRepo.deleteById(id);
	}
	public ecommerce getUserByEmail(String email) {
        
        Optional<ecommerce> optionalUser = comRepo.findByEmail(email);
        System.out.println(optionalUser);
        return optionalUser.orElse(null);
    }


	public ecommerce deleteDepartmentById(int id) {
		Optional<ecommerce> optional = comRepo.findById(id);
		
		if(optional.isPresent()){
			ecommerce ecom = optional.get();
			comRepo.delete(ecom);
			return ecom;
		}
		else{
			return null;
		}
		
	}
	public ecommerce updateByName(String name, ecommerce update){
		Optional<ecommerce> optional = comRepo.findByName(name);
		return comRepo.save(update);
	}
//	public ecommerce getByEmailAndPassword(String email, String password) {
//		
//		return comRepo.findByEmailAndPassword(email,password);
//	}


	
}
