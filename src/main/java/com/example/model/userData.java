//package com.example.model;
//
//public class userData {
//
//}

package com.example.model;

public class userData {
    private int id;
    private String name;
    private String email;
    private int number;
    private String password;

    // Getters and setters for id, name, and email

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public int getNumber() {
    	return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getPassword() {
    	return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
}
