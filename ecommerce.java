package com.example.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class ecommerce {
		@Id @GeneratedValue
		private int Id;
		private String email;
		private String password;
		private int number;
		private String name;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public ecommerce(int id, String email, String password, int number,String name) {
			super();
			Id = id;
			this.email = email;
			this.password = password;
			this.number = number;
			this.name=name;
		}
		public ecommerce (){
			
		}

		public int getId() {
			return Id;
		}



		public void setId(int id) {
			Id = id;
		}



		public String getEmail() {
			return email;
		}



		public void setEmail(String email) {
			this.email = email;
		}



		public String getPassword() {
			return password;
		}



		public void setPassword(String password) {
			this.password = password;
		}



		public int getNumber() {
			return number;
		}



		public void setNumber(int number) {
			this.number = number;
		}
		@Override
		public String toString() {
			return "ecommerce [Id=" + Id + ", email=" + email + ", password=" + password + ", number=" + number + ", name="
					+ name + "]";
		}



		


		


		


		
		
}